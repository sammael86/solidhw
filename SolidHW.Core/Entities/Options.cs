﻿namespace SolidHW.Core.Entities
{
    public class Options
    {
        public int MinimumNumber { get; init; } = 0;
        public int MaximumNumber { get; init; } = 100;
        public int Attempts { get; init; } = 5;
    }
}