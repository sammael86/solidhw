namespace SolidHW.Core.Interfaces
{
    public interface IComparator
    {
        CompareResult Compare(int firstNumber, int secondNumber);
    }
}