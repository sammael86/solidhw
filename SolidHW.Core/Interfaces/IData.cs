namespace SolidHW.Core.Interfaces
{
    public interface IData
    {
        void Write(string text);
        string Read();
    }
}