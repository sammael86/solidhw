namespace SolidHW.Core.Interfaces
{
    public interface IGame
    {
        void Start();
        void Play();
        void End();
    }
}