namespace SolidHW.Core.Interfaces
{
    public abstract class CompareResult
    {
        public abstract bool Result();

        public override string ToString()
            => "";
    }
}