namespace SolidHW.Core.Interfaces
{
    public interface IValidator
    {
        int? Validate(string str);
    }
}