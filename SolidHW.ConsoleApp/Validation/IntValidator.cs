using SolidHW.Core.Interfaces;

namespace SolidHW.ConsoleApp.Validation
{
    public class IntValidator : IValidator
    {
        public int? Validate(string str)
        {
            if (int.TryParse(str, out var intValue))
                return intValue;

            return null;
        }
    }
}