using SolidHW.Core.Interfaces;

namespace SolidHW.ConsoleApp.Comparator
{
    public class IntComparator : IComparator
    {
        public CompareResult Compare(int randomNumber, int version)
        {
            if (randomNumber > version)
                return new CompareGreater(version);

            if (randomNumber < version)
                return new CompareLess(version);

            return new CompareEqual();
        }
    }
}