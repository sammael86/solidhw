using SolidHW.Core.Interfaces;

namespace SolidHW.ConsoleApp.Comparator
{
    public class CompareLess : CompareResult
    {
        private readonly int _version;

        public CompareLess(int version)
            => _version = version;

        public override bool Result()
            => false;

        public override string ToString()
            => $"К сожалению, {_version} - это неправильный ответ. Загаданное число меньше.\n";
    }
}