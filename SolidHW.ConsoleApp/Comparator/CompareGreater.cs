using SolidHW.Core.Interfaces;

namespace SolidHW.ConsoleApp.Comparator
{
    public class CompareGreater : CompareResult
    {
        private readonly int _version;

        public CompareGreater(int version)
            => _version = version;

        public override bool Result()
            => false;

        public override string ToString()
            => $"К сожалению, {_version} - это неправильный ответ. Загаданное число больше.\n";
    }
}