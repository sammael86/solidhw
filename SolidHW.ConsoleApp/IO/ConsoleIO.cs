using System;
using SolidHW.Core.Interfaces;

namespace SolidHW.ConsoleApp.IO
{
    public class ConsoleIO : IData
    {
        public void Write(string text)
            => Console.Write(text);

        public string Read()
            => Console.ReadLine();
    }
}