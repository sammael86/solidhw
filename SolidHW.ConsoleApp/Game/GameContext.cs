using SolidHW.Core.Interfaces;

namespace SolidHW.ConsoleApp.Game
{
    public class GameContext
    {
        public int RandomNumber { get; set; }
        public IData Data { get; init; }
        public IValidator Validator { get; init; }
        public IComparator Comparator { get; init; }

        public GameContext(IData data, IValidator validator, IComparator comparator)
            => (Data, Validator, Comparator) =
                (data, validator, comparator);
    }
}