﻿using System;
using Microsoft.Extensions.Options;
using SolidHW.Core.Interfaces;
using Options = SolidHW.Core.Entities.Options;

namespace SolidHW.ConsoleApp.Game
{
    public class Game : IGame
    {
        private readonly Options _options;
        private readonly GameContext _context;

        public Game(IOptions<Options> options, GameContext context)
            => (_options, _context) =
                (options.Value, context);

        public void Start()
        {
            _context.RandomNumber = new Random().Next(_options.MinimumNumber, _options.MaximumNumber + 1);

            _context.Data.Write(
                $"Загадано число от {_options.MinimumNumber} до {_options.MaximumNumber} включительно.\n");
            _context.Data.Write($"Количество попыток для угадывания: {_options.Attempts}.\n");
        }

        public void Play()
        {
            var gameStep = new GameStep(_context);
            for (var currentAttempt = 1; currentAttempt <= _options.Attempts; currentAttempt++)
            {
                if (gameStep.MakeAttempt(currentAttempt))
                {
                    Win(currentAttempt);
                    return;
                }
            }

            Lose();
        }

        public void End()
        {
            _context.Data.Write("Спасибо, что играли в нашу замечательную игру!\n");
        }

        private void Lose()
        {
            _context.Data.Write($"Вы не смогли угадать. Было загадано число {_context.RandomNumber}.\n");
        }

        private void Win(int currentAttempt)
        {
            _context.Data.Write($"Поздравляем! Вам потребовалось попыток: {currentAttempt}.\n");
        }
    }
}