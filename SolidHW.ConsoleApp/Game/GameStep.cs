namespace SolidHW.ConsoleApp.Game
{
    public class GameStep
    {
        private readonly GameContext _context;

        public GameStep(GameContext context)
            => _context = context;

        public bool MakeAttempt(int currentAttempt)
        {
            int? version;
            do
            {
                version = GetVersion(currentAttempt);
            } while (!version.HasValue);

            return CompareVersion(version.Value);
        }

        private int? GetVersion(int currentAttempt)
        {
            _context.Data.Write($"Попытка № {currentAttempt}: ");

            var versionString = _context.Data.Read();
            var version = _context.Validator.Validate(versionString);

            if (!version.HasValue)
            {
                _context.Data.Write($"Необходимо ввести число! ");
            }

            return version;
        }

        private bool CompareVersion(int version)
        {
            var compareResult = _context.Comparator.Compare(_context.RandomNumber, version);
            _context.Data.Write(compareResult.ToString());

            return compareResult.Result();
        }
    }
}