﻿using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SolidHW.ConsoleApp.Game;
using SolidHW.Core.Entities;
using SolidHW.Core.Interfaces;

namespace SolidHW.ConsoleApp
{
    internal static class Program
    {
        private static Task Main(string[] args)
        {
            using var host = CreateHostBuilder(args).Build();

            var gameService = host.Services.GetRequiredService<IGame>();
            gameService.Start();
            gameService.Play();
            gameService.End();

            return host.StopAsync();
        }

        private static IHostBuilder CreateHostBuilder(string[] args)
            => Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                    services.Configure<Options>(hostContext.Configuration.GetSection(nameof(Options)))
                        .AddTransient<IData, IO.ConsoleIO>()
                        .AddTransient<IValidator, Validation.IntValidator>()
                        .AddTransient<IComparator, Comparator.IntComparator>()
                        .AddTransient<IGame, Game.Game>()
                        .AddTransient<GameContext>());
    }
}